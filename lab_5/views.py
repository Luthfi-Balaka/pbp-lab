from django.core import serializers
from django.http.response import JsonResponse
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    form = NoteForm(request.POST or None)
    return render(request, 'lab5_index.html', {"form": form})

def get_note(request, id):
    data = list(Note.objects.values())[int(id) - 1]
    return JsonResponse(data, safe=False)

def update_note(request, id):
    if request.is_ajax and request.method == "POST":
        form = NoteForm(request.POST or None)
        if (form.is_valid()):
            print("berhasil masuk")
            return JsonResponse()
            # instance = form.save()
            # ser_instance = serializers.serialize('json', [ instance, ])
            # return JsonResponse({"instance": ser_instance}, status=200)
    #     else:
    #         return JsonResponse({"error": form.errors}, status=400)

    # return JsonResponse({"error": ""}, status=400)