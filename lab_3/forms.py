from django import forms
from django.db.models import fields
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
	class Meta:
		model = Friend
		fields = '__all__'

	name = {
		'type' : 'text',
		'placeholder' : 'Nama Teman'
	}

	npm = {
		'type' : 'number',
		'placeholder' : 'NPM Teman'
	}

	DOB = {
		'type' : 'date'
	}
	name = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=name))
	npm = forms.IntegerField(label='', required=True,widget=forms.NumberInput(attrs=npm))
	DOB = forms.DateField(label='', required=True,widget=forms.DateInput(attrs=DOB))