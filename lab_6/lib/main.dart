import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Lab 6',
        theme: ThemeData(
          appBarTheme: const AppBarTheme(color: Color(0xFF13f4b0)),
          scaffoldBackgroundColor: const Color(0xFF1b2c54),
        ),
        home: Scaffold(
            appBar: AppBar(title: const Text('COVINDOX')),
            body: ListView(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Image.asset(
                      'images/covid.jpg',
                      fit: BoxFit.cover,
                    ),
                    const Text(
                      'Vaccination Booking Data',
                      style: TextStyle(
                        fontSize: 60,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                const Padding(padding: EdgeInsets.all(15)),
                const Text(
                  'Tanggal Vaksin: 2021-11-11',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 35,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                const Padding(padding: EdgeInsets.all(15)),
                const Text(
                  'NIK: 123456789',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 35,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                const Padding(padding: EdgeInsets.all(15)),
                const Text(
                  'Name: Luthfi',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 35,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                const Padding(padding: EdgeInsets.all(15)),
                const Text(
                  'Age: 18',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 35,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                const Padding(padding: EdgeInsets.all(15)),
              ],
            )));
  }
}
