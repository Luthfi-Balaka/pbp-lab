import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Covindox';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const RegisterVaccineForm(),
      ),
    );
  }
}

class RegisterVaccineForm extends StatefulWidget {
  const RegisterVaccineForm({Key? key}) : super(key: key);

  @override
  RegisterVaccineFormState createState() {
    return RegisterVaccineFormState();
  }
}

class RegisterVaccineFormState extends State<RegisterVaccineForm> {
  // controller untuk mengambil isi dari textformfield
  final controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // Menambahkan TextFormField untuk memasukkan input
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: Column(children: [
              TextFormField(
                controller: controller,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Input Your Full Name'),
              ),
              const SizedBox(
                height: 16,
              ),
              ElevatedButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                            content: Text('Your name: ' + controller.text));
                      });
                },
                child: const Text('Submit'),
              ),
            ])),
      ],
    );
  }
}
